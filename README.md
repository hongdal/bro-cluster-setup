# Repo for Bro-2.5.1 setup as cluster. 

First, run setup.base.sh to install dependencies. 

Second, run install.sh to install PF\_RING and Bro. 

Third, change \*.cfg files to setup Bro as cluster. This step is only required on your manager machine. worker machines should ignore this step. 

Finally, if you want to login to other machines without password do the following: 

* cd ~/.ssh/
* ssh-keygen
* cat id_rsa.pub
*\# copy this key to the target machine's ~/.ssh/authorized_keys file.

