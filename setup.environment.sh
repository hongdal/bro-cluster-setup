#!/bin/bash

# Add execution path 
echo "export PATH=\$PATH\":/usr/local/bro/bin\"" >> ~/.bashrc  
export PATH=$PATH":/usr/local/bin"

# Replace the pre-defined node.cfg file at /usr/local/bro/etc/
echo "modify and replace the pre-defined *.cfg file at /usr/local/bro/etc/"

# Install PF_RING and Bro on other workers node. I.e., run those bash files
# on other nodes.  
echo "Run setup.base.sh and install.sh  on other worker nodes."



