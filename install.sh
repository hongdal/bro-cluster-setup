#!/bin/bash

# install PF_RING
cd ./PF_RING-6.2.0/userland/lib/ 
./configure --prefix=/opt/pfring 
if [[ $? -ne 0 ]]; then 
    echo "./configure error!"
    exit 1
fi
make
if [[ $? -ne 0 ]]; then 
    echo "make error!"
    exit 1
fi
make install
if [[ $? -ne 0 ]]; then 
    echo "make install error!"
    exit 1
fi


cd ../libpcap
./configure --prefix=/opt/pfring
if [[ $? -ne 0 ]]; then 
    echo "./configure error!"
    exit 1
fi
make
if [[ $? -ne 0 ]]; then 
    echo "make error!"
    exit 1
fi
make install
if [[ $? -ne 0 ]]; then 
    echo "make install error!"
    exit 1
fi

cd ../dcpdump-4.1.1
./configure --prefix=/opt/pfring
if [[ $? -ne 0 ]]; then 
    echo "./configure error!"
    exit 1
fi
make
if [[ $? -ne 0 ]]; then 
    echo "make error!"
    exit 1
fi
make install
if [[ $? -ne 0 ]]; then 
    echo "make install error!"
    exit 1
fi

cd ../../kernel
make
if [[ $? -ne 0 ]]; then 
    echo "make error!"
    exit 1
fi
make install
if [[ $? -ne 0 ]]; then 
    echo "make install error!"
    exit 1
fi

modprobe pf_ring enable_tx_capture=0 min_num_slots=32768


# install PF_RING
cd ../../
cd ./bro-2.5.1/ 
./configure --with-pcap=/opt/pfring 
if [[ $? -ne 0 ]]; then 
    echo "./configure error!"
    exit 1
fi
make -j 20
if [[ $? -ne 0 ]]; then 
    echo "make error!"
    exit 1
fi
make install
if [[ $? -ne 0 ]]; then 
    echo "make install error!"
    exit 1
fi

# check library
echo ">>>>>>> check bro <<<<<<<<<<<<"
ldd /usr/local/bro/bin/bro | grep pcap 


echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo " Install finished successfully, source ~/.bashrc to udpate \$PATH"
echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"

