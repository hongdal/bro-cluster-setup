#!/bin/bash

apt-get update
apt-get install vim -y
apt-get install libnuma-dev -y
apt-get install cmake make gcc g++ flex bison libpcap-dev libssl-dev python-dev swig zlib1g-dev -y

tar -xf ./bro-2.5.1.tar.gz
tar -xf ./PF_RING-6.2.0.tar.gz

